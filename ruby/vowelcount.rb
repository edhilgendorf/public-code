def getCount(inputStr)
  #convert inputStr to array
  str_array=inputStr.split("")
  #define vowels
  vowels_array=Array.new
  vowels_array=["a","e","i","o","u"]
  #define total
  total=0
  str_array.each do |letter| 
    vowels_array.each do |vowel|
      if vowel == letter
        total=total+1
      end
    end
  end
  return total
end
