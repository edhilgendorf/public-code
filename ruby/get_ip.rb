class Server
    def get_ip
          'unknown ip'
    end
end

#=> nil
server = Server.new
#=> #<Server:0x007fc6bc1d4978>
server.get_ip
#=> "unknown ip"
