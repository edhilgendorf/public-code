def maskify(cc)
  # convert cc to array
  cc_array=cc.split("")
  # find length of cc
  cc_size=cc.size()
  #find how many stars needed
  cc_stars=cc_size-4
  #create answer var
  cc_answer=''
  #create array for final four
  cc_end_reverse=Array.new
  #if less than four, just return whole string
  if cc_size <= 4
    return cc_array.join
  else
    #setup first part of answer, number of stars
    cc_answer='#' * (cc_stars)
    #count down, 4 to 0
    for i in (4).downto(0)
      #push from total credit card size - i (counting down from 4) to create reversed array
      cc_end_reverse.push(cc_array[cc_size-i])
    end
    #create end string from reversed array
    cc_end_string=cc_end_reverse.join
  end
  #join stars and end string
  cc_answer=cc_answer + cc_end_string
  return cc_answer
end
