import sys
import pygame
from ship import Ship
from settings import Settings

def run_game():
    """Initialize game, create screen obj"""
    ai_settings = Settings()
    pygame.init()
    screen = pygame.display.set_mode((ai_settings.screen_width,ai_settings.screen_height))
    ship = Ship(screen)
    pygame.display.set_caption("aliens")


    #Main loop
    while True:
        screen.fill(ai_settings.bg_color)
        ship.blitme()
        #watch for keyboard or mouse
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        pygame.display.flip()

run_game()
