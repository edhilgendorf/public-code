class Settings():
    """A class to store settings"""
    def __init__(self):
        """init settings"""
        self.screen_width = 1200
        self.screen_height = 680
        self.bg_color = (230,230,230)
