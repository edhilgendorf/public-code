#!/usr/bin/python3
import unittest
from name_function import get_formatted_name

class NamesTestCase(unittest.TestCase):
    """tests for name_function.py"""
    
    def test_first_last_name(self):
        """do names like janis joplin work"""
        formatted_name = get_formatted_name('fanis', 'joplin')
        self.assertEqual(formatted_name, 'Janis Joplin')
unittest.main()
