#!/usr/bin/python3
from sys import argv

def get_formatted_name(first, last):
    """generate formatted name"""
    full_name = first + " " + last
    return full_name.title()
