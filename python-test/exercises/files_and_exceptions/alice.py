#!/usr/bin/python3

filename = "chapter_10/alice.txt"

try:
    with open(filename) as f_obj:
        contents = f_obj.read()

except FileNotFoundError:
    print("filename not found")

else:
    words = contents.split()
    num_words = len(words)
    print("the file " + filename + " has about " + str(num_words))
