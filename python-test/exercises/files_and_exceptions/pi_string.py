#!/usr/bin/python3

filename = "chapter_10/pi_million_digits.txt"
with open(filename) as file_object:
    lines = file_object.readlines()

pi_string = ""
for line in lines:
    pi_string += line.strip()

print(pi_string[:52] + "..")
print(len(pi_string))

birthday = input("your bday in form mmddyy: ")
if birthday in pi_string:
    print("your bday is in first million digits")
else:
    print("doesnt exist in pi first mill")
