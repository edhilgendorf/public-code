#!/usr/bin/python3

def count_words(filename):
    """count words in a file"""
    try:
        with open(filename) as f_obj:
            contents = f_obj.read()
    except FileNotFoundError:
        print("file not found")
    else:
        #count approx words in file
        words = contents.split()
        num_words = len(words)
        print("the file has " + str(num_words))

#filename = input("what file")
filenames = ["dsadsa","chapter_10/alice.txt","chapter_10/moby_dick.txt","chapter_10/little_women.txt"]
for filename in filenames:
    print("the book " + filename + " has ")
    count_words(filename)
