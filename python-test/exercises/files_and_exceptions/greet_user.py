#!/usr/bin/python3
from sys import argv

import json

filename = "username3.json"

try:
    with open(filename) as f_obj:
        username = json.load(f_obj)
    print("helllo " + username)

except FileNotFoundError:
    username = input("what is your name")
    with open(filename, 'w') as f_obj:
        json.dump(username, f_obj)

else:
    print("welcome back " + username)
