#!/usr/bin/python3
from sys import argv

print("enter two numbers to divide")
print("enter 'q' to quit")

while True:
    first_number = input("\nFirst number: ")
    if first_number == 'q':
        break
    second_number = input("\nSecond number:")
    if second_number == 'q':
        break
    try:
        answer=int(first_number)/int(second_number)
    except ZeroDivisionError:
        print("cant divide by 0!")
    except ValueError:
        print("cant divide by a non-numerical value!")
    else:
        print(answer)
