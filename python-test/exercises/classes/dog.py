class Dog():
    """A simple attempt to model a dog."""
    def __init__(self,name,age):
        """Initialize name and age attributes"""
        self.name = name
        self.age = age
    def sit(self):
        """simulate do g sitting"""
        print(self.name.title() + " is now sitting")
    def roll_over(self):
        """simulate rolling over"""
        print(self.name.title() + " rolled!")

my_dog = Dog('sammy',10)
print("My dog's name is " + my_dog.name.title())
print("My dog's age is " + str(my_dog.age))
my_dog.sit()
my_dog.roll_over()

print("\n" + str(my_dog))
