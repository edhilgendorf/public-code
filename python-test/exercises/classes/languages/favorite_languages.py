from collections import OrderedDict

favorite_languages = OrderedDict()

favorite_languages['edward'] = "python"
favorite_languages['andrea'] = "ruby"
favorite_languages['aussie'] = "c"

for name, language in favorite_languages.items():
    print(name.title() + "'s favorite language is " + language)
