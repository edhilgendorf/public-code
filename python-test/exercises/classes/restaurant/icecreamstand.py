#!/usr/bin/python3
from sys import argv

class Restaurant():
    def __init__(self, restaurant_name, cuisine_type):
        """initialize restaurant"""
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type
        self.people_served = 20

    def desc_restaurant(self):
        """describe restaurant"""
        print("the restaurant " + self.restaurant_name + " serves " +
              self.cuisine_type)

    def open_restaurant(self, hours):
        """open the doors"""
        print(self.restaurant_name + " is open for bizness at " + hours)

    def set_served(self, people_served):
        """set how many served today"""
        self.people_served = people_served

    def number_served(self):
        """show how manypeople served"""
        print(str(self.people_served) + " have been served today!")

    def increment_served(self, customers):
        """add another customer"""
        self.people_served += customers

class IceCreamStand(Restaurant):
    def __init__(self, restaurant_name, cuisine_type):
        """initialize ice cream"""
        super().__init__(restaurant_name, cuisine_type)
        self.flavors = ["chocolate"]
    def add_flavor(self, flavor):
        """add a flavor"""
        self.flavors.append(flavor)
    def show_flavors(self):
        """show what flavors are available"""
        for flavor in self.flavors:
            print("we have " + flavor + " available")

my_ice_cream = IceCreamStand("eddies ice cream", "ice cream")
my_ice_cream.add_flavor("strawberry")
my_ice_cream.add_flavor("vanilla")
my_ice_cream.show_flavors()
#my_restaurant = Restaurant("italian stallion", "spaghetti")
#my_restaurant.desc_restaurant()
#my_restaurant.open_restaurant("12")
#my_restaurant.number_served()
#my_restaurant.set_served(15)
#my_restaurant.number_served()
#my_restaurant.increment_served(25)
#my_restaurant.number_served()
