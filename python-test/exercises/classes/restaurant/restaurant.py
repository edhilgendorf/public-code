class Restaurant():
    def __init__(self, restaurant_name, cuisine_type):
        """initialize restaurant"""
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type
        self.people_served = 20

    def desc_restaurant(self):
        """describe restaurant"""
        print("the restaurant " + self.restaurant_name + " serves " +
              self.cuisine_type)

    def open_restaurant(self, hours):
        """open the doors"""
        print(self.restaurant_name + " is open for bizness at " + hours)

    def set_served(self, people_served):
        """set how many served today"""
        self.people_served = people_served

    def number_served(self):
        """show how manypeople served"""
        print(str(self.people_served) + " have been served today!")

    def increment_served(self, customers):
        """add another customer"""
        self.people_served += customers
