from users import User

class Privileges():
    """privileges class"""
    def __init__(self):
        """initialize privileges"""
        self.privileges = ["can add post", "can delete post"]

    def show_privileges(self):
        """method to print privileges"""
        for privilege in self.privileges:
            print("User can: " + privilege)
    def add_privilege(self, privilege):
        """add a privilege"""
        self.privileges.append(privilege)

class Admin(User):
    """desc admin user"""

    def __init__(self, first_name, last_name):
        """initialize admin"""
        super().__init__(first_name, last_name)
        self.privileges = Privileges()
