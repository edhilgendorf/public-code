#!/usr/bin/python3

class User():
    """user class"""
    def __init__(self, first_name, last_name):
        """initialize user"""
        self.first_name = first_name
        self.last_name = last_name
    
    def describe_user(self):
        """describe the user"""
        print(self.first_name.title() + " " + self.last_name.title())

    def greet_user(self):
        """greet the user"""
        print("Hello " + self.first_name.title())
