class Car():
    """simple attempt to show car"""
    def __init__(self, make, model, year):
        """"initialize attributes to describe a car"""
        self.make = make
        self.model = model
        self.year = year
        self.odometer = 0

    def get_descript_name(self):
        """return formatted name"""
        long_name = str(self.year) + ' ' + self.make + ' ' + self.model
        return long_name.title()

    def set_mileage(self, miles):
        """return formatted name"""
        if miles > self.odometer:
            self.odometer = miles
        else:
            print("miles cant go back")

    def get_mileage(self):
        """return formatted name"""
        print("This car has " + str(self.odometer) + " miles")

    def incremement_mileage(self, miles):
        """add miles to odomoteter"""
        self.odometer += miles
