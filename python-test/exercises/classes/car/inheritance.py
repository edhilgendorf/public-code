class Car():
    """simple attempt to show car"""
    def __init__(self, make, model, year):
        """"initialize attributes to describe a car"""
        self.make = make
        self.model = model
        self.year = year
        self.odometer = 0

    def get_descript_name(self):
        """return formatted name"""
        long_name = str(self.year) + ' ' + self.make + ' ' + self.model
        return long_name.title()

    def get_mileage(self):
        """return formatted name"""
        print("This car has " + str(self.odometer) + " miles")

    def fill_gastank(self, amount):
        """how much gas to put"""
        print("filling gas tank with " + str(amount) + " of gas")

    def set_mileage(self, miles):
        """return formatted name"""
        if miles > self.odometer:
            self.odometer = miles
        else:
            print("miles cant go back")

class ElectricCar(Car):
    """Represents aspects of an electric car"""
    def __init__(self, make, model, year):
        """init attributes of parent"""
        super().__init__(make, model, year)
        self.battery_size = 70
    def desc_battery(self):
        """ print statement describing battery size"""
        print("this car has a " + str(self.battery_size) + " size battery")
    def fill_gastank(slef, amount):
        """override gas method for electric"""
        print("electric cars dont use gas")

my_tesla = ElectricCar('tesla', 'model s', 2016)
print(my_tesla.get_descript_name())
my_tesla.desc_battery()
my_tesla.fill_gastank(20)
my_car = Car("honda", "fit", 2015)
my_car.fill_gastank(50)
