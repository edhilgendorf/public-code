from car import Car
from electric_car import ElectricCar

my_beetle = Car("volkswagon", "roadster", 2016)
print(my_beetle.get_descript_name())

my_tesla = ElectricCar("tesla", "s", 2014)
print(my_tesla.get_descript_name())
