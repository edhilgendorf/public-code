class Car():
    """simple attempt to show car"""
    def __init__(self, make, model, year):
        """"initialize attributes to describe a car"""
        self.make = make
        self.model = model
        self.year = year
        self.odometer = 0

    def get_descript_name(self):
        """return formatted name"""
        long_name = str(self.year) + ' ' + self.make + ' ' + self.model
        return long_name.title()


    def get_mileage(self):
        """return formatted name"""
        print("This car has " + str(self.odometer) + " miles")

my_new_car = Car('audi', 'a4', 2016)
print(my_new_car.get_descript_name())
my_new_car.get_mileage()
my_new_car.odometer = 10
my_new_car.get_mileage()
