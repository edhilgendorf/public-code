from car import Car

class Battery():
    """simple battery model"""

    def __init__(self, battery_size = 70):
        """initialize battery attributes"""
        self.battery_size = battery_size

    def describe_battery(self):
        """method to desc battery info"""
        print("this car has " + str(self.battery_size) + " sized battery")

    def get_range(self):
        """method to show range per battery"""
        if self.battery_size == 70:
            print("car can go 100 miles")
        elif self.battery_size == 80:
            print("car can go 120 miles")

class ElectricCar(Car):
    """desc an electric car"""

    def __init__(self, make, model, year):
        super().__init__(make, model, year)
        self.battery = Battery()
