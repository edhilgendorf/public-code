#!/home/edward/Dropbox/public-code/hilgendorf_dot_me/.env/bin/python3
from django.core import management

if __name__ == "__main__":
    management.execute_from_command_line()
