#!/usr/bin/env python3
__author__     = "Edward Hilgendorf"
__email__      = "ehilgendorf@sparkred.com"
__status__     = "Development"

import pprint
import string
import sys
import json

import argparse
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

def get_args():
    parser = argparse.ArgumentParser(description='Process rewrites.')
    parser.add_argument("-i","--interactive", help="Interactive mode, will pause on each redir for a retry",action="store_true")
    parser.add_argument("-v","--verbose", help="Verbose will show all redirects between source and destination",action="store_true")
    parser.add_argument("-n","--number", help="Number of Okay redirects before fail,default is 3",default=3)
    parser.add_argument("-o","--output", help="Output file to use",
                        default="output.txt")
    parser.add_argument("-F","--delimiter",default=",",help="Delimiter for input file")
    parser.add_argument("-A","--failed",help="Delimiter for input\
                        file",action="store_true")
    parser.add_argument("-f","--file", help="File to use, should be CSV. Change\
                        delimiter with -F flag")
    parser.add_argument("-u","--username", help="Username", default="admin")
    parser.add_argument("-p","--password" , help="Password", default="password")
    args=parser.parse_args()
    return args

def create_redirects(_arguments):
    redir_number=0
    redirs={}
    redirs["file"]=_arguments.file
    redirs["username"]=_arguments.username
    redirs["password"]=_arguments.password
    redirs["redirects"]={}
    redir_file = redirs["file"]
    with open(redir_file,'r') as f:
        for line in f:
            source_and_dest=line.split(str(_arguments.delimiter))
            source=source_and_dest[0]
            dest=source_and_dest[1]
            redirs["redirects"][redir_number]={}
            redirs["redirects"][redir_number]["source"]=source.replace("\n","").replace(" ","")
            redirs["redirects"][redir_number]["dest"]=dest.replace("\n","").replace(" ","")
            redir_number=redir_number+1
    return redirs

def show_history(_request_history,number_of_redirects,_redirects,counter):
    url_counter=0
    for redir in _request_history:
        print(str(redir.status_code) + ": " + redir.url)
        _redirects["redirects"][counter-1]["redirs"][url_counter]={}
        _redirects["redirects"][counter-1]["redirs"][url_counter]["status_code"]=redir.status_code
        _redirects["redirects"][counter-1]["redirs"][url_counter]["url"]=redir.url
        url_counter+=1

def is_fail(interactive,success,_counter):
    if interactive == True and success == False:
        question = "Redirect #" + str(_counter) +" failed, retry?"
        return query_yes_no(question, default="yes")

def print_results(_counter,_source):
    print("\nRule #: " + str(_counter-1))
    print("Source:   " + _source)

def test_redirects(_arguments,_redirects):
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
    retry=False
    counter=0
    for sub_dict,key in _redirects["redirects"].items():
        counter+=1
        retry=False
        first=True
        key["redirs"]={}
        while retry == True or first == True:
            first=False
            the_request=requests.Session()
            the_request.max_redirects=4;
            try:
                the_request=requests.get(key["source"],verify=False, \
                                auth=(_redirects["username"],["password"]))
            except requests.exceptions.TooManyRedirects:
                print_results(counter,key["source"])
                key["success"]=False
                print("Response: Too many redirects.")
                retry = is_fail(_arguments.interactive,key["success"],counter)
                continue
            request_history = the_request.history
            number_of_redirects=len(the_request.history)
            key["number_of_redirects"]=number_of_redirects
            key["response"]=the_request.url
            if key["dest"] == key["response"]:
              key["success"]=True
              retry=False
            else:
              key["success"]=False
              retry=True
            print_results(counter,key["source"])
            if _arguments.verbose:
                show_history(request_history,number_of_redirects,_redirects,counter)
            print("Response: "+key["response"])
            print("Dest is:  "+ key["dest"])
            retry = is_fail(_arguments.interactive,key["success"],counter)
    return _redirects

def query_yes_no(question, default="yes"):
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def print_fixed(redirects,arguments):
    redirs = redirects['redirects']
    for redir in redirs:
        uri_source=redirs[redir]['source'].split("com",1)[1]
        if redirs[redir]['success'] == False:
            rewrite = "RewriteRule ^" + uri_source + "$ " + redirs[redir]['dest'] +" [R=301,L]"
            print(rewrite,file=open(arguments.output,"a"))

def main():
    try:
        arguments=get_args()
        redirects = create_redirects(arguments)
        redirects = test_redirects(arguments,redirects)
    except Exception as e:
        print("Quitting.. dumping results:")
        print(e)
    print("\n\n")
    if arguments.failed == True:
        print_fixed(redirects,arguments)
    if arguments.verbose == False:
        del redirects['username']
        del redirects['password']
        del redirects['file']
        for redir in redirects['redirects']:
            while 'redirs' in redirects['redirects'][redir]:
                del redirects['redirects'][redir]['redirs']
                del redirects['redirects'][redir]['number_of_redirects']
    print(json.dumps(redirects,indent=4), file=open(arguments.output, "a"))

if __name__ == '__main__':
    main()
