#!/usr/bin/env python3
__author__     = "Edward Hilgendorf"
__version__    = "0.1"
__maintainer__ = "Edward Hilgendorf"
__email__      = "edward@hilgendorf.me"
__status__     = "Development"

"""     DONE: Get into version control.
	DONE: Add verbose flag functionality
        DONE: Add proper switch for username,password.
        DONE: Add flag for number of redirs allowed
        DONE: Add interactive mode flag
        DONE: Maybe accept HTTP or HTTPS, solved with n flag
        TODO: Replace global variables, refactor.
              VARIABLES: Rename them properly
              GLOBAL: replace with functions
        TODO: Add flag for seperator instead of only spaces
        TODO: Accept output file argument
        TODO: Add ALL redirects to dictionary, store info:
                  Fail or Success
                  Source
                  Destination
                  Number of redirects:
                      Redirect URL
                      Redirect Status code
              Export as JSON
"""
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import requests
import string
import sys
import argparse

#Global vars
counter=0
username=""
password=""
answer=""
failed_redirects={}
number_of_redirects=0
output_file=""
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

def query_yes_no(question, default="yes"):
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def getArgs():
    parser = argparse.ArgumentParser(description='Process rewrites.')
    parser.add_argument("-g","--suggest", help="Suggest new rewrte",action="store_true")
    parser.add_argument("-i","--interactive", help="Interactive mode, will pause on each redir for a retry",action="store_true")
    parser.add_argument("-n","--number", help="Number of Okay redirects before fail,default is 3",default=3)
    parser.add_argument("-s","--source", help="Source URL")
    parser.add_argument("-o","--output", help="Output file")
    parser.add_argument("-d","--dest", help="Dest URL")
    parser.add_argument("-F","--delimiter",default=" ",help="Delimiter for input file")
    parser.add_argument("-f","--file", help="Syntax: test_rewrites <file>\n\nFile should be space delimited, two URLS per line:\nhttp://SOURCE http://DEST")
    parser.add_argument("-u","--user", help="Username")
    parser.add_argument("-p","--password" , help="Password")
    parser.add_argument("-v","--verbose" , help="Verbosity",action="store_true")
    args=parser.parse_args()
    return args

def getFile(arguments):
    input_file = arguments.file
    print("Using file: " + input_file + "\n--------------")
    output_file=input_file+".out"
    return input_file

def mainLoop():
    global counter
    username=(arguments.user)
    password=(arguments.password)
    maximum_allowed_redirects=(int(arguments.number))
    with open(input_file, 'r') as f:
        for line in f:
            retry = False
            first = True
            theUrls=line.split(",")
            original=theUrls[0]
            new=theUrls[1].strip("\n")
            while retry == True or first == True:
                 first = False
                 theReq=requests.get(original,verify=False, auth=(username,password))
                 answer=theReq.url.replace(':443', '')
                 number_of_redirects=len(theReq.history)
                 print("\n_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_")
                 print("Rule:   " + original + " --> " + new)
                 print("Answer: " + answer)
                 print("There are " + str(number_of_redirects) + " redirects")
                 if arguments.verbose or  (number_of_redirects > maximum_allowed_redirects):
                     for redir in theReq.history:
                         print(str(redir.status_code) + ":    " + redir.url)
                 if new == answer:
                     retry=False
                 if new != answer or (number_of_redirects > maximum_allowed_redirects):
                      counter += 1
                      failed_redirects[counter]=original + "  " + new
                      find_com = original.find(".com")
                      stripped_original = original[find_com+4:]
                      if arguments.suggest == True:
                          if stripped_original[-1] == "/":
                              print("\nRule might look like:\nRewriteRule ^" + stripped_original + "?  " + new + " [L,R=301]")
                          else:
                              print("\nRule might look like:\nRewriteRule ^" + stripped_original + "  " + new + " [L,R=301]")
                      if arguments.interactive == True:
                          question = "\nFailed, retry?"
                          retry = query_yes_no(question, default="yes")

def answerSection(myOut):
    f=open(myOut, 'w')
    if counter!=0:
        print("\nRewrites not working (also added to " + myOut + "):")
    for i in failed_redirects:
            print(failed_redirects[i])
            f.write(failed_redirects[i] + "\n")

#--MAIN--#
arguments = getArgs()
input_file=getFile(arguments)
output_file=input_file+".out"
try:
    mainLoop()
except Exception as e:
    print("Rule: " + original + " has failed, please check.")
    print("Generated: " + output_file + " (failed rules)")
answerSection(output_file)
