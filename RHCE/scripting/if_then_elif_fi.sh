#!/bin/bash
if [ $1 -gt 0 ]
then
	echo "$1 is a positive"
	exit 1
elif [ $1 -eq 0 ]
then
	echo "$1 is 0"
	exit 2
elif [ $1 -lt 0 ]
then
	echo "$1 is larger than 0"
	exit 3
else
	echo "not an int"
	exit 4
fi
