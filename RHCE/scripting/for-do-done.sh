#!/bin/bash
COUNT=0
for letter in {A..Z}
do
	COUNT=`/bin/expr $COUNT + 1`
	echo "Letter $COUNT in [$letter]"
done
