#!/bin/bash

while true
do
	/usr/bin/clear
	echo " MENU"
	echo "-----"
	echo "1. Display date and time"
	echo "2. Display basic sys info"
	echo "3. Display local info"
	echo "4. Display mounted FS"
	echo "5. Exit"
	echo -e "Enter choice [1-5]: \c"
	read VAR
	case $VAR in
		1) echo "Current date/time is `/usr/bin/time`"
		   echo "press enter to go back"
		   read
		   ;;
		2) /usr/bin/hostinfo
		   echo "press enter to go back"
		   read
		   ;;
		3) echo "local info"
		   echo "press enter to go back"
		   read
		   ;;
		4) /bin/df -h
		   echo "press enter to go back"
		   read
		   ;;
		5) echo "exiting ..."
		   exit 0
		   ;;
	   *) echo "invalid"
	      echo "press enter to go back"
	      read
	      ;;
esac
done

