#!/bin/bash
for USER in user{10..14}
do
	echo "creating acc for user $USER"
	/usr/bin/useradd -m -d /home/$USER -s /bin/bash $USER
	if [ $? -eq 0 ]
	then
		echo $USER | /usr/bin/passwd -stdin $USER
		echo "$USER created"
	else
		echo "failed to create $USER"
	fi
done
